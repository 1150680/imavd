const video = document.getElementById('video');

var click = true;

document.getElementById('btn_view').addEventListener('click', () => {
    click = !click;
})

Promise.all([
    faceapi.nets.tinyFaceDetector.loadFromUri('/models'),
    faceapi.nets.faceLandmark68Net.loadFromUri('/models'),
    faceapi.nets.faceRecognitionNet.loadFromUri('/models'),
    faceapi.nets.faceExpressionNet.loadFromUri('/models'),
    faceapi.nets.ssdMobilenetv1.loadFromUri('/models'),
]).then(start);

function startVideo() {
    navigator.mediaDevices.getUserMedia({ video: {} })
    .then(function(stream) {
        video.srcObject = stream;
    })
    .catch(function(err) {
        console.log(err);
    });
}

async function start() {
    const labeledFaceDescriptors = await loadLabeledImages();
    const faceMatcher = new faceapi.FaceMatcher(labeledFaceDescriptors, 0.6);
    
    startVideo();

    video.addEventListener("playing", async () => {
		const canvas = faceapi.createCanvasFromMedia(video);
		document.body.append(canvas);

		const displaySize = { width: video.width, height: video.height };
		faceapi.matchDimensions(canvas, displaySize);

		setInterval(async () => {
            if (click)
            {
                const detections = await faceapi.detectAllFaces(video).withFaceLandmarks().withFaceDescriptors();
			    const resizedDetections = faceapi.resizeResults(detections, displaySize);

			    canvas.getContext("2d").clearRect(0, 0, canvas.width, canvas.height);

			    const results = resizedDetections.map(d => faceMatcher.findBestMatch(d.descriptor));

                results.forEach((result, i) => {
                    const box = resizedDetections[i].detection.box
                    const drawBox = new faceapi.draw.DrawBox(box, { label: result.toString() })
                    drawBox.draw(canvas)
                });
            }
            else
            {
                const detections = await faceapi.detectAllFaces(video, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks().withFaceExpressions();
			    const resizedDetections = faceapi.resizeResults(detections, displaySize);

			    canvas.getContext("2d").clearRect(0, 0, canvas.width, canvas.height);

                faceapi.draw.drawDetections(canvas, resizedDetections);
                faceapi.draw.drawFaceLandmarks(canvas, resizedDetections);
                faceapi.draw.drawFaceExpressions(canvas, resizedDetections);
            }
		});
	}, 100)
}

function loadLabeledImages() {
    //const labels = ['Tiago Faria', 'Tony Stark'];
    const labels = ['Tiago Faria', 'Miguel Pinto', 'Nuno Silva']
    return Promise.all(
        labels.map(async label => {
            const descriptions = [];
            for (let i = 1; i <= 2; i++) {
                const img = await faceapi.fetchImage(
                    `https://bitbucket.org/1150680/imavd/raw/HEAD/IMAVD_Video/labeled_images/${label}/${i}.jpg`
                    );
                const detections = await faceapi.detectSingleFace(img).withFaceLandmarks().withFaceDescriptor();
                descriptions.push(detections.descriptor);
            }

            return new faceapi.LabeledFaceDescriptors(label, descriptions);
        })
    )
}