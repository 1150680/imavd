﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using System.Windows.Media;
using NAudio;
using NAudio.Wave;

namespace IMAVD
{
    public partial class Form1 : Form
    {

        String loadedPath = "";
        bool recording = false;
        WaveIn waveSource = null;
        WaveFileWriter waveFile = null;
        bool recordingExists = false;
        string fileName;
        Bitmap bmp;

        byte image_id;


        public Form1()
        {
            InitializeComponent();
            image_id = 0;
        }

        private void loadFileButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Audio files(*.wav; *.aiff; *.pcm; *.flac; *.alac; *.wma; *.mp3; *.ogg; *.aac)| *.wav; *.aiff; *.pcm; *.flac; *.alac; *.wma; *.mp3; *.ogg; *.aac";
            if (open.ShowDialog() == DialogResult.OK)
            {
                loadedPath = open.FileName;
            }

            if (!loadedPath.Equals(""))
            {
                playButton.Enabled = true;
                transcribeButton.Enabled = true;
            }
        }

        private void playButton_Click(object sender, EventArgs e)
        {
            var reader = new MediaFoundationReader(loadedPath);
            var waveOut = new WaveOut();
            waveOut.Init(reader);
            waveOut.Play();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!recording)
            {
                recordingExists = false;

                waveSource = new WaveIn();
                waveSource.WaveFormat = new WaveFormat(44100, 1);

                waveSource.DataAvailable += new EventHandler<WaveInEventArgs>(waveSource_DataAvailable);
                waveSource.RecordingStopped += new EventHandler<StoppedEventArgs>(waveSource_RecordingStopped);

                waveFile = new WaveFileWriter(@"recording.wav", waveSource.WaveFormat);

                waveSource.StartRecording();

                recordingStatus.Visible = true;
                recording = true;
            }
            else
            {
                waveSource.StopRecording();
                waveFile.Close();

                recordingStatus.Visible = false;

                String text = convertToText(@"recording.wav");

                textBox1.Text = text;

                recording = false;
            }
        }

        String convertToText(String path)
        {
            String temp;
            String[] tokens;
            String fileName;
            String res;

            using (WebClient client = new WebClient())
            {

                client.Encoding = Encoding.UTF8;

                byte[] response = client.UploadFile("https://figure-out-backend.herokuapp.com/files/convert", path);
                temp = client.Encoding.GetString(response);

                
    
                tokens = temp.Split('"');
                fileName = tokens[3];

                NameValueCollection test = new NameValueCollection
                {
                    {"fileName", fileName },
                    {"origin_language", "en" },
                    {"target_language", "en" }
                };

                byte[] response2 = client.UploadValues("https://figure-out-backend.herokuapp.com/translations/audio", "POST", test);
                temp = client.Encoding.GetString(response2);
                tokens = temp.Split('"');
                res = tokens[3];
            }

            Console.WriteLine(res);

            CheckVoiceCommands(res);

            return res;
        }

        public void CheckVoiceCommands(String Message)
        {

            String message_copy = Message.ToLower();
            switch (message_copy)
            {
#region change image
                case "please change to square":
                    {
                        image_id = 1;
                        pictureBox2.Image = Properties.Resources.Square_Blue;
                        bmp = (Bitmap)pictureBox2.Image;
                    }
                    break;

                case "please change square":
                    {
                        image_id = 1;
                        pictureBox2.Image = Properties.Resources.Square_Blue;
                        bmp = (Bitmap)pictureBox2.Image;
                    }
                    break;

                case "change to square":
                {
                    image_id = 1;
                    pictureBox2.Image = Properties.Resources.Square_Blue;
                    bmp = (Bitmap)pictureBox2.Image;
                }
                break;

                case "change square":
                {
                    image_id = 1;
                    pictureBox2.Image = Properties.Resources.Square_Blue;
                    bmp = (Bitmap)pictureBox2.Image;
                    }
                break;


                case "please change to triangle":
                    {
                        image_id = 2;
                        pictureBox2.Image = Properties.Resources.Triangle_Red;
                        bmp = (Bitmap)pictureBox2.Image;
                    }
                    break;


                case "please change triangle":
                    {
                        image_id = 2;
                        pictureBox2.Image = Properties.Resources.Triangle_Red;
                        bmp = (Bitmap)pictureBox2.Image;
                    }
                    break;

                case "change to triangle":
                {
                    image_id = 2;
                    pictureBox2.Image = Properties.Resources.Triangle_Red;
                    bmp = (Bitmap)pictureBox2.Image;
                }
                break;
                case "change triangle":
                {
                    image_id = 2;
                    pictureBox2.Image = Properties.Resources.Triangle_Red;
                        bmp = (Bitmap)pictureBox2.Image;
                    }
                break;

                case "please change to circle":
                    {
                        image_id = 3;
                        pictureBox2.Image = Properties.Resources.Circle_Green;
                        bmp = (Bitmap)pictureBox2.Image;
                    }
                    break;


                case "please change circle":
                    {
                        image_id = 3;
                        pictureBox2.Image = Properties.Resources.Circle_Green;
                        bmp = (Bitmap)pictureBox2.Image;
                    }
                    break;

                case "change to circle":
                    {
                        image_id = 3;
                        pictureBox2.Image = Properties.Resources.Circle_Green;
                    }
                    break;
                case "change circle":
                    {
                        image_id = 3;
                        pictureBox2.Image = Properties.Resources.Circle_Green;
                    }
                    break;

                #endregion

#region change color
                case "square red":
                {
                    if(image_id == 1)
                    {
                       pictureBox2.Image = Properties.Resources.Square_Red;
                       bmp = (Bitmap)pictureBox2.Image;
                    }
                }
                break;

                case "square blue":
                {
                    if (image_id == 1)
                    {
                       pictureBox2.Image = Properties.Resources.Square_Blue;
                            bmp = (Bitmap)pictureBox2.Image;
                        }
                }
                break;

                case "square green":
                {
                    if (image_id == 1)
                    {
                       pictureBox2.Image = Properties.Resources.Square_Green;
                            bmp = (Bitmap)pictureBox2.Image;
                        }
                }
                break;

                case "triangle red":
                    {
                        if (image_id == 2)
                        {
                            pictureBox2.Image = Properties.Resources.Triangle_Red;
                            bmp = (Bitmap)pictureBox2.Image;
                        }
                    }
                    break;

                case "triangle blue":
                    {
                        if (image_id == 2)
                        {
                            pictureBox2.Image = Properties.Resources.Triangle_Blue;
                            bmp = (Bitmap)pictureBox2.Image;
                        }
                    }
                    break;

                case "triangle green":
                    {
                        if (image_id == 2)
                        {
                            pictureBox2.Image = Properties.Resources.Triangle_Green;
                            bmp = (Bitmap)pictureBox2.Image;
                        }
                    }
                    break;

                case "circle red":
                    {
                        if (image_id == 3)
                        {
                            pictureBox2.Image = Properties.Resources.Circle_Red;
                            bmp = (Bitmap)pictureBox2.Image;
                        }
                    }
                    break;

                case "circle blue":
                    {
                        if (image_id == 3)
                        {
                            pictureBox2.Image = Properties.Resources.Circle_Blue;
                            bmp = (Bitmap)pictureBox2.Image;
                        }
                    }
                    break;

                case "circle green":
                    {
                        if (image_id == 3)
                        {
                            pictureBox2.Image = Properties.Resources.Circle_Green;
                            bmp = (Bitmap)pictureBox2.Image;
                        }
                    }
                    break;

                #endregion

#region rotate image

                case "rotate square left":
                {
                    if (image_id == 1)
                    {
                        RotateLeft();
                    }
                }
                break;

                case "rotate square right":
                {
                    if(image_id == 1)
                    {
                        RotateRight();
                    }
                   
                }
                break;

                case "square right":
                    {
                        if (image_id == 1)
                        {
                            RotateRight();
                        }

                    }
                    break;

                case "square left":
                    {
                        if (image_id == 1)
                        {
                            RotateLeft();
                        }

                    }
                    break;


                case "square left rotate":
                    {
                        if (image_id == 1)
                        {
                            RotateLeft();
                        }

                    }
                    break;


                case "square right rotate":
                    {
                        if (image_id == 1)
                        {
                            RotateLeft();
                        }

                    }
                    break;

                case "rotate triangle right":
                {
                    if (image_id == 2)
                    {
                        RotateRight();
                    }

                }
                    break;

                case "triangle right rotate":
                    {
                        if (image_id == 2)
                        {
                            RotateRight();
                        }

                    }
                    break;


                case "triangle right":
                    {
                        if (image_id == 2)
                        {
                            RotateRight();
                        }

                    }
                    break;

                case "rotate triangle left":
                {
                    if (image_id == 2)
                    {
                        RotateLeft();
                    }

                }
                break;


                case "triangle left rotate":
                    {
                        if (image_id == 2)
                        {
                            RotateLeft();
                        }

                    }
                    break;


                case "triangle left":
                    {
                        if (image_id == 2)
                        {
                            RotateLeft();
                        }
                    }
                    break;


                case "rotate circle right":
                    {
                        if (image_id == 3)
                        {
                            RotateRight();
                        }

                    }
                    break;

                case "circle right rotate":
                    {
                        if (image_id == 3)
                        {
                            RotateRight();
                        }

                    }
                    break;


                case "circle right":
                    {
                        if (image_id == 2)
                        {
                            RotateRight();
                        }

                    }
                    break;

                case "rotate circle left":
                    {
                        if (image_id == 2)
                        {
                            RotateLeft();
                        }

                    }
                    break;


                case "circle left rotate":
                    {
                        if (image_id == 2)
                        {
                            RotateLeft();
                        }

                    }
                    break;


                case "circle left":
                    {
                        if (image_id == 2)
                        {
                            RotateLeft();
                        }
                    }
                    break;

                    #endregion

                    //--------------------

                    //circle 3   
                //triangle 2   
                //suqare 1
                #region image scaling

                case "bigger square":
                    {
                        if (image_id == 1)
                        {
                            //
                        }
                    }
                    break;

                case "bigger circle":
                    {
                        if (image_id == 3)
                        {
                            //
                        }

                    }
                    break;

                case "bigger triangle":
                    {
                        if (image_id == 2)
                        {
                            //
                        }

                    }
                    break;


                case "square bigger":
                    {
                        if (image_id == 1)
                        {
                            //
                        }
                    }
                    break;

                case "circle bigger":
                    {
                        if (image_id == 3)
                        {
                            //
                        }

                    }
                    break;

                case "triangle bigger":
                    {
                        if (image_id == 2)
                        {
                            //
                        }

                    }
                    break;

                case "smaller square":
                    {
                        if (image_id == 1)
                        {
                            //
                        }
                    }
                    break;

                case "smaller circle":
                    {
                        if (image_id == 3)
                        {
                            //
                        }

                    }
                    break;

                case "smaller triangle":
                    {
                        if (image_id == 2)
                        {
                            //
                        }

                    }
                    break;


                case "square smaller":
                    {
                        if (image_id == 1)
                        {
                            //
                        }
                    }
                    break;

                case "circle smaller":
                    {
                        if (image_id == 3)
                        {
                            //
                        }

                    }
                    break;

                case "triangle smaller":
                    {
                        if (image_id == 2)
                        {
                            //
                        }

                    }
                    break;
                #endregion




                //--------------------------------

                default:
                {
             
                }
                break;

            }   
        }

        private async Task<System.IO.Stream> Upload(string actionUrl, string paramString, Stream paramFileStream, byte[] paramFileBytes)
        {
            HttpContent stringContent = new StringContent(paramString);
            HttpContent fileStreamContent = new StreamContent(paramFileStream);
            HttpContent bytesContent = new ByteArrayContent(paramFileBytes);
            using (var client = new HttpClient())
            using (var formData = new MultipartFormDataContent())
            {
                formData.Add(stringContent, "param1", "param1");
                formData.Add(fileStreamContent, "file1", "file1");
                formData.Add(bytesContent, "file2", "file2");
                var response = await client.PostAsync(actionUrl, formData);
                if (!response.IsSuccessStatusCode)
                {
                    return null;
                }
                return await response.Content.ReadAsStreamAsync();
            }
        }

        void waveSource_DataAvailable(object sender, WaveInEventArgs e)
        {
            if (waveFile != null)
            {
                waveFile.Write(e.Buffer, 0, e.BytesRecorded);
                waveFile.Flush();

                recordingExists = true;
            }
        }

        void waveSource_RecordingStopped(object sender, StoppedEventArgs e)
        {
            if (waveSource != null)
            {
                waveSource.Dispose();
                waveSource = null;
            }

            if (waveFile != null)
            {
                waveFile.Dispose();
                waveFile = null;
            }

        }

        public async void WaitSomeTime()
        {
            await Task.Delay(5000);
            this.Enabled = true;
            this.Cursor = Cursors.Default;
        }

        private void transcribeButton_Click(object sender, EventArgs e)
        {
            String text = convertToText(loadedPath);

            textBox1.Text = text;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Open Image";
                dlg.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png"; ;

                if (dlg.ShowDialog() == DialogResult.OK)
                {

                    pictureBox2.Image = new Bitmap(dlg.FileName);
                    fileName = dlg.FileName;

                    Bitmap bmp = new Bitmap(pictureBox2.Image);
                    Graphics g = Graphics.FromImage(bmp);

                    g.DrawImage(bmp, new Point(0, 0));
                    g.Dispose();

                    pictureBox2.Image = bmp;

                }
            }

            bmp = (Bitmap)pictureBox2.Image;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        public void RotateRight()
        {
            bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
            pictureBox2.Image = bmp;
        }

        public void RotateLeft()
        {
            bmp.RotateFlip(RotateFlipType.Rotate270FlipNone);
            pictureBox2.Image = bmp;
        }

        private void button2_Click(object sender, EventArgs e)
        {
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
