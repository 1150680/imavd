﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class ImageDetails : Form
    {
        Bitmap bmp;
        string fileName;

        public ImageDetails(Bitmap bmp, string name)
        {
            InitializeComponent();

            this.bmp = bmp;
            fileName = name;

            Lbl_Name.Text = name.Split('\\')[name.Split('\\').Length - 1].Split('.')[0];
            Lbl_Extension.Text = "." + name.Split('.')[1];
            Lbl_Location.Text = name.Replace("\\" + Lbl_Name.Text + Lbl_Extension.Text, "");
            Lbl_Dimension.Text = bmp.Width.ToString() + " x " + bmp.Height.ToString();
            Lbl_Size.Text = (new FileInfo(fileName).Length / 1024).ToString() + " KB";
            Lbl_CreatedOn.Text = (new FileInfo(fileName).CreationTime).ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
