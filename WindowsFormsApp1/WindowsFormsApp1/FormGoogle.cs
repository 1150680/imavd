﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class FormGoogle : Form
    {
        Bitmap bmp;
        string fileName;
        int brightness = 0;

        public FormGoogle()
        {
            InitializeComponent();
            bmp = null;
            fileName = null;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Open Image";
                //dlg.Filter = "bmp files (*.bmp)|*.bmp";

                if (dlg.ShowDialog() == DialogResult.OK)
                {

                    pictureBox1.Image = new Bitmap(dlg.FileName);
                    fileName = dlg.FileName;

                    Bitmap bmp = new Bitmap(pictureBox1.Image);
                    Graphics g = Graphics.FromImage(bmp);

                    g.DrawImage(bmp, new Point(0, 0));
                    g.Dispose();

                    pictureBox1.Image = bmp;

                }
            }

            bmp = (Bitmap)pictureBox1.Image;
        }

        private void Btn_Details_Click(object sender, EventArgs e)
        {
            if (fileName != null && bmp != null)
                new ImageDetails(bmp, fileName).ShowDialog();
        }

        private void newImage()
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Bitmap adjustedImage = new Bitmap(bmp.Width, bmp.Height);
            //float brightness = 1.0f; // no change in brightness
            //float contrast = 1.0f; // twice the contrast
            //float gamma = 1.0f; // no change in gamma

            //float inserted_contrast = (float) Num_Contrast.Value;

            //contrast = contrast + (inserted_contrast / 100);
            //tb_Test.Text = contrast.ToString();

            //float adjustedBrightness = brightness - 1.0f;
            //// create matrix that will brighten and contrast the image
            //float[][] ptsArray ={
            //new float[] {contrast, 0f, 0f, 0f, 0f}, // scale red
            //new float[] {0f, contrast, 0f, 0f, 0f}, // scale green
            //new float[] {0f, 0f, contrast, 0f, 0f}, // scale blue
            //new float[] {0f, 0f, 0f, 1.0f, 0f}, // don't scale alpha
            //new float[] { 0.001f, 0.001f, 0.001f, 0f, 1f}};

            //ImageAttributes imageAttributes = new ImageAttributes();
            //imageAttributes.ClearColorMatrix();
            //imageAttributes.SetColorMatrix(new ColorMatrix(ptsArray), ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
            //imageAttributes.SetGamma(gamma, ColorAdjustType.Bitmap);
            //Graphics g = Graphics.FromImage(adjustedImage);
            //g.DrawImage(bmp, new Rectangle(0, 0, bmp.Width, bmp.Height), 0, 0, bmp.Width, bmp.Height, GraphicsUnit.Pixel, imageAttributes);
            //g.Dispose();
            //imageAttributes.Dispose();
            //pictureBox2.Image = adjustedImage;
        }

        private void test()
        {
            //int i, j, nr, ng, nb;
            //Bitmap bmp2 = new Bitmap(fileName);

            //for (i = 0; i < bmp2.Width; i++)
            //{
            //    for (j = 0; j < bmp2.Height; j++)
            //    {
            //        Color clr = bmp2.GetPixel(i, j);

            //        double C = ((100.0 + (Convert.ToInt32(Num_Contrast.Value) - 100)) / 100.0) * ((100.0 + (Convert.ToInt32(Num_Contrast.Value) - 100)) / 100.0);

            //        double temp = ((((clr.R / 255.0) - 0.5) * C) + 0.5) * 255.0;
            //        nr = (int)temp;
            //        temp = ((((clr.G / 255.0) - 0.5) * C) + 0.5) * 255.0;
            //        ng = (int)temp;
            //        temp = ((((clr.B / 255.0) - 0.5) * C) + 0.5) * 255.0;
            //        nb = (int)temp;

            //        if (nr < 0) { nr = 0; }
            //        if (nr > 255) { nr = 255; }
            //        if (ng < 0) { ng = 0; }
            //        if (ng > 255) { ng = 255; }
            //        if (nb < 0) { nb = 0; }
            //        if (nb > 255) { nb = 255; }

            //        bmp2.SetPixel(i, j, Color.FromArgb(clr.A, nr, ng, nb));
            //        pictureBox2.Image = bmp2;
            //    }
            //}
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //Bitmap adjustedImage = new Bitmap(bmp.Width, bmp.Height);
            //float brightness = 1.0f; // no change in brightness
            //float contrast = 1.0f; // twice the contrast
            //float gamma = 1.0f; // no change in gamma

            //float inserted_contrast = (float)Num_Contrast.Value;

            //contrast = contrast + (inserted_contrast / 100);
            //tb_Test.Text = contrast.ToString();

            //float adjustedBrightness = brightness - 1.0f;
            //// create matrix that will brighten and contrast the image
            //float[][] ptsArray ={
            //new float[] {contrast, 0f, 0f, 0f, 0f}, // scale red
            //new float[] {0f, contrast, 0f, 0f, 0f}, // scale green
            //new float[] {0f, 0f, contrast, 0f, 0f}, // scale blue
            //new float[] {0f, 0f, 0f, 1.0f, 0f}, // don't scale alpha
            //new float[] { 0.001f, 0.001f, 0.001f, 0f, 1f}};

            //ImageAttributes imageAttributes = new ImageAttributes();
            //imageAttributes.ClearColorMatrix();
            //imageAttributes.SetColorMatrix(new ColorMatrix(ptsArray), ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
            //imageAttributes.SetGamma(gamma, ColorAdjustType.Bitmap);
            //Graphics g = Graphics.FromImage(adjustedImage);
            //g.DrawImage(bmp, new Rectangle(0, 0, bmp.Width, bmp.Height), 0, 0, bmp.Width, bmp.Height, GraphicsUnit.Pixel, imageAttributes);
            //g.Dispose();
            //imageAttributes.Dispose();

            //pictureBox2.Image = adjustedImage;
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            label3.Text = trackBar1.Value.ToString();

            pictureBox1.Image = AdjustBrightnessContrast(bmp, trackBar1.Value, trackBar2.Value);
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            label4.Text = trackBar2.Value.ToString();

            pictureBox1.Image = AdjustBrightnessContrast(bmp, trackBar1.Value, trackBar2.Value);
        }

        public static Bitmap AdjustBrightnessContrast(Bitmap image, int bright, int cont)
        {
            Bitmap TempBitmap = image;

            float finalValue = bright / 255.0f;

            Bitmap newBitmap = new Bitmap(TempBitmap.Width, TempBitmap.Height);

            Graphics newGraphics = Graphics.FromImage(newBitmap);

            float contrast;

            if (cont == 0)
                contrast = 1f;
            else
                contrast = 0.04f * cont;

            float[][] ptsArray ={
            new float[] {contrast, 0, 0, 0, 0}, // scale red
            new float[] {0, contrast, 0, 0, 0}, // scale green
            new float[] {0, 0, contrast, 0, 0}, // scale blue
            new float[] {0, 0, 0, contrast, 0}, // don't scale alpha
            new float[] { finalValue, finalValue, finalValue, 1, 1}};

            ColorMatrix newColorMatrix = new ColorMatrix(ptsArray);

            ImageAttributes imageAttributes = new ImageAttributes();

            imageAttributes.SetColorMatrix(newColorMatrix);

            newGraphics.DrawImage(TempBitmap, new Rectangle(0, 0, TempBitmap.Width, TempBitmap.Height), 0, 0, TempBitmap.Width, TempBitmap.Height, GraphicsUnit.Pixel, imageAttributes);

            newGraphics.Dispose();
            imageAttributes.Dispose();

            return newBitmap;
        }

        public static Bitmap AdjustContrast(Bitmap image, int value)
        {
            Bitmap TempBitmap = image;

            float contrast = value * 0.04f;

            Bitmap newBitmap = new Bitmap(TempBitmap.Width, TempBitmap.Height);

            Graphics newGraphics = Graphics.FromImage(newBitmap);

            float[][] ptsArray ={
            new float[] {contrast, 0f, 0f, 0f, 0f}, // scale red
            new float[] {0f, contrast, 0f, 0f, 0f}, // scale green
            new float[] {0f, 0f, contrast, 0f, 0f}, // scale blue
            new float[] {0f, 0f, 0f, contrast, 0f}, // don't scale alpha
            new float[] { 0.001f, 0.001f, 0.001f, 1f, 0f}};

            ColorMatrix newColorMatrix = new ColorMatrix(ptsArray);

            ImageAttributes imageAttributes = new ImageAttributes();

            imageAttributes.SetColorMatrix(newColorMatrix);

            newGraphics.DrawImage(TempBitmap, new Rectangle(0, 0, TempBitmap.Width, TempBitmap.Height), 0, 0, TempBitmap.Width, TempBitmap.Height, GraphicsUnit.Pixel, imageAttributes);

            newGraphics.Dispose();
            imageAttributes.Dispose();

            return newBitmap;
        }
    }
}
