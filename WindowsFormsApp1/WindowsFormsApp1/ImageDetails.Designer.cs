﻿namespace WindowsFormsApp1
{
    partial class ImageDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Lbl_Name = new System.Windows.Forms.Label();
            this.Lbl_Extension = new System.Windows.Forms.Label();
            this.Lbl_Location = new System.Windows.Forms.Label();
            this.Lbl_Dimension = new System.Windows.Forms.Label();
            this.Lbl_Size = new System.Windows.Forms.Label();
            this.Lbl_CreatedOn = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Image Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Image Extension:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Image Dimension:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 164);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Image Size:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 92);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Image Location:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 199);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(127, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "Image Created On:";
            // 
            // Lbl_Name
            // 
            this.Lbl_Name.AutoSize = true;
            this.Lbl_Name.Location = new System.Drawing.Point(145, 19);
            this.Lbl_Name.Name = "Lbl_Name";
            this.Lbl_Name.Size = new System.Drawing.Size(0, 17);
            this.Lbl_Name.TabIndex = 6;
            // 
            // Lbl_Extension
            // 
            this.Lbl_Extension.AutoSize = true;
            this.Lbl_Extension.Location = new System.Drawing.Point(145, 55);
            this.Lbl_Extension.Name = "Lbl_Extension";
            this.Lbl_Extension.Size = new System.Drawing.Size(0, 17);
            this.Lbl_Extension.TabIndex = 7;
            // 
            // Lbl_Location
            // 
            this.Lbl_Location.AutoSize = true;
            this.Lbl_Location.Location = new System.Drawing.Point(145, 94);
            this.Lbl_Location.Name = "Lbl_Location";
            this.Lbl_Location.Size = new System.Drawing.Size(0, 17);
            this.Lbl_Location.TabIndex = 8;
            // 
            // Lbl_Dimension
            // 
            this.Lbl_Dimension.AutoSize = true;
            this.Lbl_Dimension.Location = new System.Drawing.Point(145, 128);
            this.Lbl_Dimension.Name = "Lbl_Dimension";
            this.Lbl_Dimension.Size = new System.Drawing.Size(0, 17);
            this.Lbl_Dimension.TabIndex = 9;
            // 
            // Lbl_Size
            // 
            this.Lbl_Size.AutoSize = true;
            this.Lbl_Size.Location = new System.Drawing.Point(145, 164);
            this.Lbl_Size.Name = "Lbl_Size";
            this.Lbl_Size.Size = new System.Drawing.Size(0, 17);
            this.Lbl_Size.TabIndex = 10;
            // 
            // Lbl_CreatedOn
            // 
            this.Lbl_CreatedOn.AutoSize = true;
            this.Lbl_CreatedOn.Location = new System.Drawing.Point(145, 199);
            this.Lbl_CreatedOn.Name = "Lbl_CreatedOn";
            this.Lbl_CreatedOn.Size = new System.Drawing.Size(0, 17);
            this.Lbl_CreatedOn.TabIndex = 11;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(135, 249);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 32);
            this.button1.TabIndex = 12;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ImageDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(497, 307);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Lbl_CreatedOn);
            this.Controls.Add(this.Lbl_Size);
            this.Controls.Add(this.Lbl_Dimension);
            this.Controls.Add(this.Lbl_Location);
            this.Controls.Add(this.Lbl_Extension);
            this.Controls.Add(this.Lbl_Name);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ImageDetails";
            this.Text = "ImageDetails";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label Lbl_Name;
        private System.Windows.Forms.Label Lbl_Extension;
        private System.Windows.Forms.Label Lbl_Location;
        private System.Windows.Forms.Label Lbl_Dimension;
        private System.Windows.Forms.Label Lbl_Size;
        private System.Windows.Forms.Label Lbl_CreatedOn;
        private System.Windows.Forms.Button button1;
    }
}