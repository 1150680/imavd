﻿namespace WindowsFormsApp1
{
    partial class ColorDetectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Btn_ChooseImage = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.btn_PickColor = new System.Windows.Forms.Button();
            this.Btn_DetectColor = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.ImagePathLbl_label = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panelSelectedColor = new System.Windows.Forms.Panel();
            this.SelectedColorName_label = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.xeesus_btn = new System.Windows.Forms.Button();
            this.google_btn = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.Btn_Details = new System.Windows.Forms.Button();
            this.Lbl_Brightness = new System.Windows.Forms.Label();
            this.Lbl_Contrast = new System.Windows.Forms.Label();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.trackBar2 = new System.Windows.Forms.TrackBar();
            this.Lbl_BrightnessValue = new System.Windows.Forms.Label();
            this.Lbl_ContrastValue = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).BeginInit();
            this.SuspendLayout();
            // 
            // Btn_ChooseImage
            // 
            this.Btn_ChooseImage.Location = new System.Drawing.Point(29, 34);
            this.Btn_ChooseImage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Btn_ChooseImage.Name = "Btn_ChooseImage";
            this.Btn_ChooseImage.Size = new System.Drawing.Size(140, 28);
            this.Btn_ChooseImage.TabIndex = 1;
            this.Btn_ChooseImage.Text = "Choose Image";
            this.Btn_ChooseImage.UseVisualStyleBackColor = true;
            this.Btn_ChooseImage.Click += new System.EventHandler(this.ChooseImageBtn_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(604, 15);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(523, 483);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 38);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(308, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Pick the color which you want to detect in Image";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // colorDialog1
            // 
            this.colorDialog1.AllowFullOpen = false;
            this.colorDialog1.SolidColorOnly = true;
            // 
            // btn_PickColor
            // 
            this.btn_PickColor.Location = new System.Drawing.Point(29, 82);
            this.btn_PickColor.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_PickColor.Name = "btn_PickColor";
            this.btn_PickColor.Size = new System.Drawing.Size(136, 28);
            this.btn_PickColor.TabIndex = 4;
            this.btn_PickColor.Text = "Pick Color";
            this.btn_PickColor.UseVisualStyleBackColor = true;
            this.btn_PickColor.Click += new System.EventHandler(this.PickColorBtn_Click);
            // 
            // Btn_DetectColor
            // 
            this.Btn_DetectColor.Location = new System.Drawing.Point(325, 158);
            this.Btn_DetectColor.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Btn_DetectColor.Name = "Btn_DetectColor";
            this.Btn_DetectColor.Size = new System.Drawing.Size(136, 28);
            this.Btn_DetectColor.TabIndex = 5;
            this.Btn_DetectColor.Text = "Detect Color";
            this.Btn_DetectColor.UseVisualStyleBackColor = true;
            this.Btn_DetectColor.Click += new System.EventHandler(this.DetectColorBtn_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // ImagePathLbl_label
            // 
            this.ImagePathLbl_label.AutoSize = true;
            this.ImagePathLbl_label.ForeColor = System.Drawing.Color.Magenta;
            this.ImagePathLbl_label.Location = new System.Drawing.Point(165, 81);
            this.ImagePathLbl_label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ImagePathLbl_label.Name = "ImagePathLbl_label";
            this.ImagePathLbl_label.Size = new System.Drawing.Size(0, 17);
            this.ImagePathLbl_label.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 164);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 17);
            this.label2.TabIndex = 8;
            this.label2.Text = "Selected Color : ";
            // 
            // panelSelectedColor
            // 
            this.panelSelectedColor.Location = new System.Drawing.Point(169, 164);
            this.panelSelectedColor.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panelSelectedColor.Name = "panelSelectedColor";
            this.panelSelectedColor.Size = new System.Drawing.Size(72, 16);
            this.panelSelectedColor.TabIndex = 10;
            // 
            // SelectedColorName_label
            // 
            this.SelectedColorName_label.AutoSize = true;
            this.SelectedColorName_label.ForeColor = System.Drawing.Color.Magenta;
            this.SelectedColorName_label.Location = new System.Drawing.Point(283, 164);
            this.SelectedColorName_label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.SelectedColorName_label.Name = "SelectedColorName_label";
            this.SelectedColorName_label.Size = new System.Drawing.Size(0, 17);
            this.SelectedColorName_label.TabIndex = 11;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.groupBox1.Controls.Add(this.Btn_Details);
            this.groupBox1.Controls.Add(this.Btn_ChooseImage);
            this.groupBox1.Controls.Add(this.ImagePathLbl_label);
            this.groupBox1.Location = new System.Drawing.Point(16, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(551, 113);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Image Picker";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.groupBox2.Controls.Add(this.Btn_DetectColor);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.btn_PickColor);
            this.groupBox2.Controls.Add(this.SelectedColorName_label);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.panelSelectedColor);
            this.groupBox2.Location = new System.Drawing.Point(16, 154);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(551, 293);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Color Picker";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.groupBox3.Location = new System.Drawing.Point(16, 497);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Size = new System.Drawing.Size(551, 70);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Color Detector";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.groupBox4.Controls.Add(this.textBox1);
            this.groupBox4.Location = new System.Drawing.Point(16, 369);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox4.Size = new System.Drawing.Size(551, 70);
            this.groupBox4.TabIndex = 15;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Number of Pixels per color";
            this.groupBox4.Enter += new System.EventHandler(this.groupBox4_Enter_2);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(188, 23);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(132, 22);
            this.textBox1.TabIndex = 1;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(16, 688);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 28);
            this.button1.TabIndex = 16;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // xeesus_btn
            // 
            this.xeesus_btn.Location = new System.Drawing.Point(341, 612);
            this.xeesus_btn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.xeesus_btn.Name = "xeesus_btn";
            this.xeesus_btn.Size = new System.Drawing.Size(100, 28);
            this.xeesus_btn.TabIndex = 17;
            this.xeesus_btn.Text = "Xeesus";
            this.xeesus_btn.UseVisualStyleBackColor = true;
            this.xeesus_btn.Click += new System.EventHandler(this.xeesus_btn_Click);
            // 
            // google_btn
            // 
            this.google_btn.Location = new System.Drawing.Point(481, 612);
            this.google_btn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.google_btn.Name = "google_btn";
            this.google_btn.Size = new System.Drawing.Size(100, 28);
            this.google_btn.TabIndex = 18;
            this.google_btn.Text = "Google";
            this.google_btn.UseVisualStyleBackColor = true;
            this.google_btn.Click += new System.EventHandler(this.google_btn_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(185, 23);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(185, 30);
            this.button2.TabIndex = 19;
            this.button2.Text = "Write Text in Image";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.groupBox5.Controls.Add(this.button2);
            this.groupBox5.Location = new System.Drawing.Point(16, 756);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox5.Size = new System.Drawing.Size(551, 70);
            this.groupBox5.TabIndex = 16;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Save Image atfer to Plat effect";
            // 
            // Btn_Details
            // 
            this.Btn_Details.Location = new System.Drawing.Point(29, 70);
            this.Btn_Details.Name = "Btn_Details";
            this.Btn_Details.Size = new System.Drawing.Size(140, 28);
            this.Btn_Details.TabIndex = 8;
            this.Btn_Details.Text = "Image Details";
            this.Btn_Details.UseVisualStyleBackColor = true;
            this.Btn_Details.Click += new System.EventHandler(this.Btn_Details_Click);
            // 
            // Lbl_Brightness
            // 
            this.Lbl_Brightness.AutoSize = true;
            this.Lbl_Brightness.Location = new System.Drawing.Point(601, 521);
            this.Lbl_Brightness.Name = "Lbl_Brightness";
            this.Lbl_Brightness.Size = new System.Drawing.Size(75, 17);
            this.Lbl_Brightness.TabIndex = 19;
            this.Lbl_Brightness.Text = "Brightness";
            // 
            // Lbl_Contrast
            // 
            this.Lbl_Contrast.AutoSize = true;
            this.Lbl_Contrast.Location = new System.Drawing.Point(601, 583);
            this.Lbl_Contrast.Name = "Lbl_Contrast";
            this.Lbl_Contrast.Size = new System.Drawing.Size(61, 17);
            this.Lbl_Contrast.TabIndex = 20;
            this.Lbl_Contrast.Text = "Contrast";
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(682, 505);
            this.trackBar1.Maximum = 255;
            this.trackBar1.Minimum = -255;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(384, 56);
            this.trackBar1.TabIndex = 21;
            this.trackBar1.TickFrequency = 20;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // trackBar2
            // 
            this.trackBar2.Location = new System.Drawing.Point(682, 567);
            this.trackBar2.Maximum = 100;
            this.trackBar2.Name = "trackBar2";
            this.trackBar2.Size = new System.Drawing.Size(384, 56);
            this.trackBar2.TabIndex = 22;
            this.trackBar2.TickFrequency = 10;
            this.trackBar2.Value = 25;
            this.trackBar2.Scroll += new System.EventHandler(this.trackBar2_Scroll);
            // 
            // Lbl_BrightnessValue
            // 
            this.Lbl_BrightnessValue.AutoSize = true;
            this.Lbl_BrightnessValue.Location = new System.Drawing.Point(1072, 521);
            this.Lbl_BrightnessValue.Name = "Lbl_BrightnessValue";
            this.Lbl_BrightnessValue.Size = new System.Drawing.Size(16, 17);
            this.Lbl_BrightnessValue.TabIndex = 23;
            this.Lbl_BrightnessValue.Text = "0";
            // 
            // Lbl_ContrastValue
            // 
            this.Lbl_ContrastValue.AutoSize = true;
            this.Lbl_ContrastValue.Location = new System.Drawing.Point(1072, 583);
            this.Lbl_ContrastValue.Name = "Lbl_ContrastValue";
            this.Lbl_ContrastValue.Size = new System.Drawing.Size(24, 17);
            this.Lbl_ContrastValue.TabIndex = 24;
            this.Lbl_ContrastValue.Text = "25";
            // 
            // ColorDetectForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1149, 969);
            this.Controls.Add(this.Lbl_ContrastValue);
            this.Controls.Add(this.Lbl_BrightnessValue);
            this.Controls.Add(this.trackBar2);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.Lbl_Contrast);
            this.Controls.Add(this.Lbl_Brightness);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.google_btn);
            this.Controls.Add(this.xeesus_btn);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ColorDetectForm";
            this.Text = "Color Detector";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Btn_ChooseImage;
        private System.Windows.Forms.Button btn_PickColor;
        private System.Windows.Forms.Button Btn_DetectColor;

        private System.Windows.Forms.PictureBox pictureBox1;

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label SelectedColorName_label;
        private System.Windows.Forms.Label ImagePathLbl_label;

        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;

        private System.Windows.Forms.Panel panelSelectedColor;

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button xeesus_btn;
        private System.Windows.Forms.Button google_btn;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button Btn_Details;
        private System.Windows.Forms.Label Lbl_Brightness;
        private System.Windows.Forms.Label Lbl_Contrast;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.TrackBar trackBar2;
        private System.Windows.Forms.Label Lbl_BrightnessValue;
        private System.Windows.Forms.Label Lbl_ContrastValue;
    }
}

