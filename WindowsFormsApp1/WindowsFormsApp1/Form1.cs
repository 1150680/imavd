﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;

namespace WindowsFormsApp1
{
    public partial class ColorDetectForm : Form
    {

        string gammaValue;
        Bitmap bmp;
        Color actualColor;
        int number_pixels_color;
        string fileName;

        public ColorDetectForm()
        {
            InitializeComponent();
            actualColor = panelSelectedColor.BackColor;
            number_pixels_color = 0;
            bmp = null;
        }

        private void ChooseImageBtn_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Open Image";
                dlg.Filter = "bmp files (*.bmp)|*.bmp";

                if (dlg.ShowDialog() == DialogResult.OK)
                {

                    pictureBox1.Image = new Bitmap(dlg.FileName);
                    fileName = dlg.FileName;

                    Bitmap bmp = new Bitmap(pictureBox1.Image);
                    Graphics g = Graphics.FromImage(bmp);

                    g.DrawImage(bmp, new Point(0, 0));
                    g.Dispose();

                    pictureBox1.Image = bmp;

                }
            }

            bmp = (Bitmap)pictureBox1.Image;
        }

        private void PickColorBtn_Click(object sender, EventArgs e)
        {
            try
            {
                //Clear previously selected color
                SelectedColorName_label.Text = "";
                panelSelectedColor.BackColor = actualColor;

                //Showing color selected by the user
                DialogResult IsColorChosen = colorDialog1.ShowDialog();

                if (IsColorChosen == System.Windows.Forms.DialogResult.OK)
                {
                    panelSelectedColor.BackColor = colorDialog1.Color;

                    //If it is a known color, display the color name  
                    if (colorDialog1.Color.IsKnownColor == true)
                    {
                        SelectedColorName_label.Text = colorDialog1.Color.ToKnownColor().ToString();
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void DetectColorBtn_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean IsColorFound = false;

                if (pictureBox1.Image != null)
                {
                    //Converting selected image into bitmap
                    Bitmap bmp = new Bitmap(pictureBox1.Image);

                    //Search for the color in the bitmap, for all the pixels
                    for (int i = 0; i < pictureBox1.Image.Height; i++)
                    {
                        for (int j = 0; j < pictureBox1.Image.Width; j++)
                        {
                            //Get the color at each pixel
                            Color now_color = bmp.GetPixel(j, i);

                            //Compare the selected color ARGB to a Pixel's Color ARGB property
                            if (now_color.ToArgb() == colorDialog1.Color.ToArgb())
                            {
                                number_pixels_color++;
                                IsColorFound = true;

                            }
                        }
                        if (IsColorFound == true)
                        {
                            Console.WriteLine(number_pixels_color);
                            textBox1.Text = number_pixels_color.ToString();
                            MessageBox.Show("Color Found!");
                            break;
                        }
                    }

                    if (IsColorFound == false)
                    {
                        MessageBox.Show("Selected Color Not Found.");
                    }
                }
                else
                {
                    MessageBox.Show("Image has not been loaded");
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }


        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }


        private void groupBox4_Enter_2(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            textBox1.Text = number_pixels_color.ToString();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            SaveFileDialog sxf = new SaveFileDialog();
            sxf.Filter = "JPG(*.JPG|*.jpg";

            if (sxf.ShowDialog() == DialogResult.OK)
            {
                bmp.Save(sxf.FileName);
            }
        }

        private void xeesus_btn_Click(object sender, EventArgs e)
        {
            new FormXeesus().ShowDialog();
        }

        private void google_btn_Click(object sender, EventArgs e)
        {
            new FormGoogle().ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string firstWord = "Hello";
            string secondWord = "World";


            PointF firstWordLocation = new PointF(40f, 30f);
            PointF secondWordLocation = new PointF(40f, 70f);


            using (Graphics graphics = Graphics.FromImage(bmp))
            {
                using (Font arialFont = new Font("Arial", 10))
                {
                    graphics.DrawString(firstWord, arialFont, Brushes.Blue, firstWordLocation);
                    graphics.DrawString(secondWord, arialFont, Brushes.Red, secondWordLocation);
                }
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void Btn_Details_Click(object sender, EventArgs e)
        {
            if (fileName != null && bmp != null)
                new ImageDetails(bmp, fileName).ShowDialog();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            Lbl_BrightnessValue.Text = trackBar1.Value.ToString();

            pictureBox1.Image = AdjustBrightnessContrast(bmp, trackBar1.Value, trackBar2.Value);
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            Lbl_ContrastValue.Text = trackBar2.Value.ToString();

            pictureBox1.Image = AdjustBrightnessContrast(bmp, trackBar1.Value, trackBar2.Value);
        }

        public static Bitmap AdjustBrightnessContrast(Bitmap image, int bright, int cont)
        {
            Bitmap TempBitmap = image;

            float finalValue = bright / 255.0f;

            Bitmap newBitmap = new Bitmap(TempBitmap.Width, TempBitmap.Height);

            Graphics newGraphics = Graphics.FromImage(newBitmap);

            float contrast = 0.04f * cont;

            float[][] ptsArray ={
            new float[] {contrast, 0, 0, 0, 0}, // scale red
            new float[] {0, contrast, 0, 0, 0}, // scale green
            new float[] {0, 0, contrast, 0, 0}, // scale blue
            new float[] {0, 0, 0, contrast, 0}, // don't scale alpha
            new float[] { finalValue, finalValue, finalValue, 1, 1}};

            ColorMatrix newColorMatrix = new ColorMatrix(ptsArray);

            ImageAttributes imageAttributes = new ImageAttributes();

            imageAttributes.SetColorMatrix(newColorMatrix);

            newGraphics.DrawImage(TempBitmap, new Rectangle(0, 0, TempBitmap.Width, TempBitmap.Height), 0, 0, TempBitmap.Width, TempBitmap.Height, GraphicsUnit.Pixel, imageAttributes);

            newGraphics.Dispose();
            imageAttributes.Dispose();

            return newBitmap;
        }
    }
}
