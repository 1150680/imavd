﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class FormXeesus : Form
    {
        public Bitmap bmp;
        public int originalHeight;
        public int originalWidth;
        public string originalFileName;

        public FormXeesus()
        {
            InitializeComponent();
            bmp = null;
            originalHeight = 0;
            originalWidth = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //SaveFileDialog sxf = new SaveFileDialog();
            //sxf.Filter = "JPG(*.JPG|*.jpg";

            //if (sxf.ShowDialog() == DialogResult.OK)
            //{
            //    bmp.Save(sxf.FileName);
            //}

            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Open Image";
                dlg.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png"; ;

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    originalFileName = dlg.FileName;
                    pictureBox1.Image = new Bitmap(dlg.FileName);

                    Bitmap bmp = new Bitmap(pictureBox1.Image);
                    Graphics g = Graphics.FromImage(bmp);

                    g.DrawImage(bmp, new Point(0, 0));
                    g.Dispose();

                    originalHeight = bmp.Height;
                    originalWidth = bmp.Width;

                    pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;

                    //pictureBox1.Image = bmp;

                }
            }

            bmp = (Bitmap)pictureBox1.Image;
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            //150 zoom

            int new_width = (int)(originalWidth * 1.5);
            int new_height = (int)(originalHeight* 1.5);
            Bitmap temp_bmp = new Bitmap(bmp, new_width,new_height);
            Graphics gpu = Graphics.FromImage(temp_bmp);
            gpu.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            pictureBox1.Image = null;
            pictureBox1.Image = temp_bmp;
            pictureBox1.Size = temp_bmp.Size;
            //pictureBox1.SizeMode = PictureBoxSizeMode.
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            //50 zoom
            int new_width = (int)(originalWidth * 0.5);
            int new_height = (int)(originalHeight * 0.5);
            Bitmap temp_bmp = new Bitmap(bmp, new Size(new_width, new_height));
            Graphics gpu = Graphics.FromImage(temp_bmp);
            gpu.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            pictureBox1.Image = null;
            pictureBox1.Image = temp_bmp;
            pictureBox1.Size = temp_bmp.Size;
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            //100 zoom
            int new_width = originalWidth;
            int new_height = originalHeight;
            Bitmap temp_bmp = new Bitmap(bmp, new Size(new_width, new_height));
            Graphics gpu = Graphics.FromImage(temp_bmp);
            gpu.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            pictureBox1.Image = null;
            pictureBox1.Image = temp_bmp;
            pictureBox1.Size = temp_bmp.Size;
        }

        private void redFilterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitmap rbmp = new Bitmap(bmp);

            for(int y = 0; y < originalHeight; y++)
            {
                for(int x = 0; x < originalWidth; x++)
                {
                    Color p = bmp.GetPixel(x, y);

                    int a = p.A;
                    int r = p.R;

                    rbmp.SetPixel(x, y, Color.FromArgb(a, r, 0, 0));
                }
            }

            pictureBox1.Image = rbmp;
        }

        private void greenFilterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitmap gbmp = new Bitmap(bmp);

            for (int y = 0; y < originalHeight; y++)
            {
                for (int x = 0; x < originalWidth; x++)
                {
                    Color p = bmp.GetPixel(x, y);

                    int a = p.A;
                    int g = p.G;

                    gbmp.SetPixel(x, y, Color.FromArgb(a, 0, g, 0));
                }
            }

            pictureBox1.Image = gbmp;
        }

        private void blueFilterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitmap bbmp = new Bitmap(bmp);

            for (int y = 0; y < originalHeight; y++)
            {
                for (int x = 0; x < originalWidth; x++)
                {
                    Color p = bmp.GetPixel(x, y);

                    int a = p.A;
                    int b = p.B;

                    bbmp.SetPixel(x, y, Color.FromArgb(a, 0, 0, b));
                }
            }

            pictureBox1.Image = bbmp;
        }

        private void chromaKeyRemoverToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitmap output_bmp = new Bitmap(bmp);
            // Iterate over all piels from top to bottom...
            for (int y = 0; y < originalHeight; y++)
            {
                // ...and from left to right
                for (int x = 0; x < originalWidth; x++)
                {
                    // Determine the pixel color
                    Color camColor = bmp.GetPixel(x, y);

                    // Every component (red, green, and blue) can have a value from 0 to 255, so determine the extremes
                    byte max = Math.Max(Math.Max(camColor.R, camColor.G), camColor.B);
                    byte min = Math.Min(Math.Min(camColor.R, camColor.G), camColor.B);

                    // Should the pixel be masked/replaced?
                    bool replace =
                        camColor.G != min // green is not the smallest value
                        && (camColor.G == max // green is the biggest value
                        || max - camColor.G < 8) // or at least almost the biggest value
                        && (max - min) > 96; // minimum difference between smallest/biggest value (avoid grays)

                    if (replace)
                        camColor = Color.Magenta;

                    // Set the output pixel
                    output_bmp.SetPixel(x, y, camColor);
                }
            }

            pictureBox1.Image = output_bmp;
        }

        private void ºToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (bmp != null)
            {
                bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
                pictureBox1.Image = bmp;
            }
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            if (bmp != null)
            {
                bmp.RotateFlip(RotateFlipType.Rotate180FlipNone);
                pictureBox1.Image = bmp;
            }
        }

        private void invertColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitmap ibmp = new Bitmap(bmp);

            for (int y = 0; y < originalHeight; y++)
            {
                for (int x = 0; x < originalWidth; x++)
                {
                    Color p = bmp.GetPixel(x, y);

                    Color invertedColor = Color.FromArgb(p.ToArgb() ^ 0xffffff);

                    ibmp.SetPixel(x, y, invertedColor);
                }
            }

            pictureBox1.Image = ibmp;
        }
    }
}
