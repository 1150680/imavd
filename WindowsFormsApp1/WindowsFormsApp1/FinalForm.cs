﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class FinalForm : Form
    {
        int number_pixels_color;
        public Bitmap bmp;
        public int originalHeight;
        public int originalWidth;
        public string originalFileName;
        string gammaValue;
        float gamma;
        Color actualColor;

        public FinalForm()
        {
            InitializeComponent();
            bmp = null;
            originalHeight = 0;
            originalWidth = 0;
            originalFileName = "";
        }

        private void openImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Open Image";
                dlg.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png"; ;

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    originalFileName = dlg.FileName;
                    pictureBox1.Image = new Bitmap(dlg.FileName);

                    Bitmap bmp = new Bitmap(pictureBox1.Image);
                    Graphics g = Graphics.FromImage(bmp);

                    g.DrawImage(bmp, new Point(0, 0));
                    g.Dispose();

                    originalHeight = bmp.Height;
                    originalWidth = bmp.Width;

                    pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;

                    //pictureBox1.Image = bmp;

                }
            }

            bmp = (Bitmap)pictureBox1.Image;
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            //50 zoom
            int new_width = (int)(originalWidth * 0.5);
            int new_height = (int)(originalHeight * 0.5);
            Bitmap temp_bmp = new Bitmap(bmp, new Size(new_width, new_height));
            Graphics gpu = Graphics.FromImage(temp_bmp);
            gpu.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            pictureBox1.Image = null;
            pictureBox1.Image = temp_bmp;
            pictureBox1.Size = temp_bmp.Size;
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            //100 zoom
            int new_width = originalWidth;
            int new_height = originalHeight;
            Bitmap temp_bmp = new Bitmap(bmp, new Size(new_width, new_height));
            Graphics gpu = Graphics.FromImage(temp_bmp);
            gpu.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            pictureBox1.Image = null;
            pictureBox1.Image = temp_bmp;
            pictureBox1.Size = temp_bmp.Size;
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            //150 zoom
            int new_width = (int)(originalWidth * 1.5);
            int new_height = (int)(originalHeight * 1.5);
            Bitmap temp_bmp = new Bitmap(bmp, new_width, new_height);
            Graphics gpu = Graphics.FromImage(temp_bmp);
            gpu.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            pictureBox1.Image = null;
            pictureBox1.Image = temp_bmp;
            pictureBox1.Size = temp_bmp.Size;
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //200 zoom
            int new_width = (int)(originalWidth * 2);
            int new_height = (int)(originalHeight * 2);
            Bitmap temp_bmp = new Bitmap(bmp, new_width, new_height);
            Graphics gpu = Graphics.FromImage(temp_bmp);
            gpu.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            pictureBox1.Image = null;
            pictureBox1.Image = temp_bmp;
            pictureBox1.Size = temp_bmp.Size;
        }

        private void redFilterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitmap rbmp = new Bitmap(bmp);

            for (int y = 0; y < originalHeight; y++)
            {
                for (int x = 0; x < originalWidth; x++)
                {
                    Color p = bmp.GetPixel(x, y);

                    int a = p.A;
                    int r = p.R;

                    rbmp.SetPixel(x, y, Color.FromArgb(a, r, 0, 0));
                }
            }

            pictureBox1.Image = rbmp;
        }

        private void blueFilterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitmap bbmp = new Bitmap(bmp);

            for (int y = 0; y < originalHeight; y++)
            {
                for (int x = 0; x < originalWidth; x++)
                {
                    Color p = bmp.GetPixel(x, y);

                    int a = p.A;
                    int b = p.B;

                    bbmp.SetPixel(x, y, Color.FromArgb(a, 0, 0, b));
                }
            }

            pictureBox1.Image = bbmp;
        }

        private void greenFilterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitmap gbmp = new Bitmap(bmp);

            for (int y = 0; y < originalHeight; y++)
            {
                for (int x = 0; x < originalWidth; x++)
                {
                    Color p = bmp.GetPixel(x, y);

                    int a = p.A;
                    int g = p.G;

                    gbmp.SetPixel(x, y, Color.FromArgb(a, 0, g, 0));
                }
            }

            pictureBox1.Image = gbmp;
        }

        private void chromakeyRemoverToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitmap output_bmp = new Bitmap(bmp);
            // Iterate over all piels from top to bottom...
            for (int y = 0; y < originalHeight; y++)
            {
                // ...and from left to right
                for (int x = 0; x < originalWidth; x++)
                {
                    // Determine the pixel color
                    Color camColor = bmp.GetPixel(x, y);

                    // Every component (red, green, and blue) can have a value from 0 to 255, so determine the extremes
                    byte max = Math.Max(Math.Max(camColor.R, camColor.G), camColor.B);
                    byte min = Math.Min(Math.Min(camColor.R, camColor.G), camColor.B);

                    // Should the pixel be masked/replaced?
                    bool replace =
                        camColor.G != min // green is not the smallest value
                        && (camColor.G == max // green is the biggest value
                        || max - camColor.G < 8) // or at least almost the biggest value
                        && (max - min) > 96; // minimum difference between smallest/biggest value (avoid grays)

                    if (replace)
                        camColor = Color.Magenta;

                    // Set the output pixel
                    output_bmp.SetPixel(x, y, camColor);
                }
            }

            pictureBox1.Image = output_bmp;
        }

        private void degreesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (bmp != null)
            {
                bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
                pictureBox1.Image = bmp;
                originalHeight = bmp.Height;
                originalWidth= bmp.Width;
            }
        }

        private void degreesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (bmp != null)
            {
                bmp.RotateFlip(RotateFlipType.Rotate180FlipNone);
                pictureBox1.Image = bmp;
                originalHeight = bmp.Height;
                originalWidth = bmp.Width;
            }
        }

        private void colorDetectionToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void pickColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                //Clear previously selected color
                SelectedColorName_label.Text = "";
                panelSelectedColor.BackColor = actualColor;

                //Showing color selected by the user
                DialogResult IsColorChosen = colorDialog1.ShowDialog();

                if (IsColorChosen == System.Windows.Forms.DialogResult.OK)
                {
                    //panelSelectedColor.BackColor = colorDialog1.Color;

                    //If it is a known color, display the color name  
                    if (colorDialog1.Color.IsKnownColor == true)
                    {
                        SelectedColorName_label.Text = colorDialog1.Color.ToKnownColor().ToString();
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void btn_PickColor_Click(object sender, EventArgs e)
        {

        }

        private void pressToDetectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean IsColorFound = false;

                if (pictureBox1.Image != null)
                {
                    //Converting selected image into bitmap
                    Bitmap bmp = new Bitmap(pictureBox1.Image);

                    //Search for the color in the bitmap, for all the pixels
                    for (int i = 0; i < pictureBox1.Image.Height; i++)
                    {
                        for (int j = 0; j < pictureBox1.Image.Width; j++)
                        {
                            //Get the color at each pixel
                            Color now_color = bmp.GetPixel(j, i);

                            //Compare the selected color ARGB to a Pixel's Color ARGB property
                            if (now_color.ToArgb() == colorDialog1.Color.ToArgb())
                            {
                                number_pixels_color++;
                                IsColorFound = true;

                            }
                        }
                        if (IsColorFound == true)
                        {
                            Console.WriteLine(number_pixels_color);
                            textBox1.Text = number_pixels_color.ToString();
                            MessageBox.Show("Color Found!");
                            break;
                        }
                    }

                    if (IsColorFound == false)
                    {
                        MessageBox.Show("Selected Color Not Found.");
                    }
                }
                else
                {
                    MessageBox.Show("Image has not been loaded");
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void writeTextInImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //need to save it atfer to see the effects
            string firstWord = "Hello";
            string secondWord = "World";


            PointF firstWordLocation = new PointF(40f, 30f);
            PointF secondWordLocation = new PointF(40f, 70f);


            using (Graphics graphics = Graphics.FromImage(bmp))
            {
                using (Font arialFont = new Font("Arial", 10))
                {
                    graphics.DrawString(firstWord, arialFont, Brushes.Blue, firstWordLocation);
                    graphics.DrawString(secondWord, arialFont, Brushes.Red, secondWordLocation);
                }
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sxf = new SaveFileDialog();
            sxf.Filter = "JPG(*.JPG|*.jpg";

            if (sxf.ShowDialog() == DialogResult.OK)
            {
                bmp.Save(sxf.FileName);
            }
        }

        private void invertColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitmap ibmp = new Bitmap(bmp);

            for (int y = 0; y < originalHeight; y++)
            {
                for (int x = 0; x < originalWidth; x++)
                {
                    Color p = bmp.GetPixel(x, y);

                    Color invertedColor = Color.FromArgb(p.ToArgb() ^ 0xffffff);

                    ibmp.SetPixel(x, y, invertedColor);
                }
            }
            pictureBox1.Image = ibmp;
        }

        private Bitmap changeGamma()
        {
            
            System.Drawing.Imaging.ImageAttributes attributes = new System.Drawing.Imaging.ImageAttributes();
            attributes.SetGamma(gamma);

            Point[] points =
            {
                new Point(0, 0),
                new Point(pictureBox1.Image.Width, 0),
                new Point(0, pictureBox1.Image.Height),
            };

            Rectangle rect = new Rectangle(0, 0, pictureBox1.Image.Width, pictureBox1.Image.Height);

            Bitmap bm = new Bitmap(pictureBox1.Image.Width, pictureBox1.Image.Height);
            using (Graphics gr = Graphics.FromImage(bm))
            {
                gr.DrawImage(pictureBox1.Image, points, rect, GraphicsUnit.Pixel, attributes);
            }

            return bm;
        }

        

        private void gammaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            changeGamma();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            TextBox objTextBox = (TextBox)sender;
            gammaValue = objTextBox.Text;

            gamma = float.Parse(gammaValue, System.Globalization.CultureInfo.InvariantCulture);
        }

        private void label2_Click_1(object sender, EventArgs e)
        {

        }

        

        public static Bitmap AdjustBrightnessContrast(Bitmap image, int bright, int cont)
        {
            Bitmap TempBitmap = image;

            float finalValue = bright / 255.0f;

            Bitmap newBitmap = new Bitmap(TempBitmap.Width, TempBitmap.Height);

            Graphics newGraphics = Graphics.FromImage(newBitmap);

            float contrast = 0.04f * cont;

            float[][] ptsArray ={
            new float[] {contrast, 0, 0, 0, 0}, // scale red
            new float[] {0, contrast, 0, 0, 0}, // scale green
            new float[] {0, 0, contrast, 0, 0}, // scale blue
            new float[] {0, 0, 0, contrast, 0}, // don't scale alpha
            new float[] { finalValue, finalValue, finalValue, 1, 1}};

            ColorMatrix newColorMatrix = new ColorMatrix(ptsArray);

            ImageAttributes imageAttributes = new ImageAttributes();

            imageAttributes.SetColorMatrix(newColorMatrix);

            newGraphics.DrawImage(TempBitmap, new Rectangle(0, 0, TempBitmap.Width, TempBitmap.Height), 0, 0, TempBitmap.Width, TempBitmap.Height, GraphicsUnit.Pixel, imageAttributes);

            newGraphics.Dispose();
            imageAttributes.Dispose();

            return newBitmap;
        }

        private void viewImageDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (originalFileName != null && bmp != null)
                new ImageDetails(bmp, originalFileName).ShowDialog();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            Lbl_BrightnessValue.Text = trackBar1.Value.ToString();

            if (originalFileName != null && bmp != null)
                pictureBox1.Image = AdjustBrightnessContrast(bmp, trackBar1.Value, trackBar2.Value);
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            Lbl_ContrastValue.Text = trackBar2.Value.ToString();

            if (originalFileName != null && bmp != null)
                pictureBox1.Image = AdjustBrightnessContrast(bmp, trackBar1.Value, trackBar2.Value);
        }
    }
}
