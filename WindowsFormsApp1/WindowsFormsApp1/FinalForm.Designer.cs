﻿namespace WindowsFormsApp1
{
    partial class FinalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zoomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.colorFilterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redFilterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blueFilterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.greenFilterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chromakeyRemoverToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.degreesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.degreesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.colorDetectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pickColorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pressToDetectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeTextInImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.invertColorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gammaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewImageDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.SelectedColorName_label = new System.Windows.Forms.Label();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.panelSelectedColor = new System.Windows.Forms.Panel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Lbl_Brightness = new System.Windows.Forms.Label();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.Lbl_BrightnessValue = new System.Windows.Forms.Label();
            this.Lbl_Contrast = new System.Windows.Forms.Label();
            this.trackBar2 = new System.Windows.Forms.TrackBar();
            this.Lbl_ContrastValue = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1067, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openImageToolStripMenuItem,
            this.saveToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openImageToolStripMenuItem
            // 
            this.openImageToolStripMenuItem.Name = "openImageToolStripMenuItem";
            this.openImageToolStripMenuItem.Size = new System.Drawing.Size(166, 26);
            this.openImageToolStripMenuItem.Text = "Open Image";
            this.openImageToolStripMenuItem.Click += new System.EventHandler(this.openImageToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(166, 26);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.zoomToolStripMenuItem,
            this.colorFilterToolStripMenuItem,
            this.chromakeyRemoverToolStripMenuItem,
            this.rotateToolStripMenuItem,
            this.colorDetectionToolStripMenuItem,
            this.writeTextInImageToolStripMenuItem,
            this.invertColorToolStripMenuItem,
            this.gammaToolStripMenuItem,
            this.viewImageDetailsToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(56, 24);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // zoomToolStripMenuItem
            // 
            this.zoomToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4,
            this.toolStripMenuItem1,
            this.toolStripMenuItem6});
            this.zoomToolStripMenuItem.Name = "zoomToolStripMenuItem";
            this.zoomToolStripMenuItem.Size = new System.Drawing.Size(221, 26);
            this.zoomToolStripMenuItem.Text = "Zoom";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(120, 26);
            this.toolStripMenuItem2.Text = "50%";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(120, 26);
            this.toolStripMenuItem3.Text = "100%";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(120, 26);
            this.toolStripMenuItem4.Text = "150%";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(120, 26);
            this.toolStripMenuItem1.Text = "200%";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(120, 26);
            this.toolStripMenuItem6.Text = "300%";
            // 
            // colorFilterToolStripMenuItem
            // 
            this.colorFilterToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.redFilterToolStripMenuItem,
            this.blueFilterToolStripMenuItem,
            this.greenFilterToolStripMenuItem});
            this.colorFilterToolStripMenuItem.Name = "colorFilterToolStripMenuItem";
            this.colorFilterToolStripMenuItem.Size = new System.Drawing.Size(221, 26);
            this.colorFilterToolStripMenuItem.Text = "ColorFilter";
            // 
            // redFilterToolStripMenuItem
            // 
            this.redFilterToolStripMenuItem.Name = "redFilterToolStripMenuItem";
            this.redFilterToolStripMenuItem.Size = new System.Drawing.Size(160, 26);
            this.redFilterToolStripMenuItem.Text = "Red Filter";
            this.redFilterToolStripMenuItem.Click += new System.EventHandler(this.redFilterToolStripMenuItem_Click);
            // 
            // blueFilterToolStripMenuItem
            // 
            this.blueFilterToolStripMenuItem.Name = "blueFilterToolStripMenuItem";
            this.blueFilterToolStripMenuItem.Size = new System.Drawing.Size(160, 26);
            this.blueFilterToolStripMenuItem.Text = "Blue Filter";
            this.blueFilterToolStripMenuItem.Click += new System.EventHandler(this.blueFilterToolStripMenuItem_Click);
            // 
            // greenFilterToolStripMenuItem
            // 
            this.greenFilterToolStripMenuItem.Name = "greenFilterToolStripMenuItem";
            this.greenFilterToolStripMenuItem.Size = new System.Drawing.Size(160, 26);
            this.greenFilterToolStripMenuItem.Text = "Green Filter";
            this.greenFilterToolStripMenuItem.Click += new System.EventHandler(this.greenFilterToolStripMenuItem_Click);
            // 
            // chromakeyRemoverToolStripMenuItem
            // 
            this.chromakeyRemoverToolStripMenuItem.Name = "chromakeyRemoverToolStripMenuItem";
            this.chromakeyRemoverToolStripMenuItem.Size = new System.Drawing.Size(221, 26);
            this.chromakeyRemoverToolStripMenuItem.Text = "Chromakey Remover";
            this.chromakeyRemoverToolStripMenuItem.Click += new System.EventHandler(this.chromakeyRemoverToolStripMenuItem_Click);
            // 
            // rotateToolStripMenuItem
            // 
            this.rotateToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.degreesToolStripMenuItem,
            this.degreesToolStripMenuItem1});
            this.rotateToolStripMenuItem.Name = "rotateToolStripMenuItem";
            this.rotateToolStripMenuItem.Size = new System.Drawing.Size(221, 26);
            this.rotateToolStripMenuItem.Text = "Rotate";
            // 
            // degreesToolStripMenuItem
            // 
            this.degreesToolStripMenuItem.Name = "degreesToolStripMenuItem";
            this.degreesToolStripMenuItem.Size = new System.Drawing.Size(165, 26);
            this.degreesToolStripMenuItem.Text = "90 degrees";
            this.degreesToolStripMenuItem.Click += new System.EventHandler(this.degreesToolStripMenuItem_Click);
            // 
            // degreesToolStripMenuItem1
            // 
            this.degreesToolStripMenuItem1.Name = "degreesToolStripMenuItem1";
            this.degreesToolStripMenuItem1.Size = new System.Drawing.Size(165, 26);
            this.degreesToolStripMenuItem1.Text = "180 degrees";
            this.degreesToolStripMenuItem1.Click += new System.EventHandler(this.degreesToolStripMenuItem1_Click);
            // 
            // colorDetectionToolStripMenuItem
            // 
            this.colorDetectionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pickColorToolStripMenuItem,
            this.pressToDetectToolStripMenuItem});
            this.colorDetectionToolStripMenuItem.Name = "colorDetectionToolStripMenuItem";
            this.colorDetectionToolStripMenuItem.Size = new System.Drawing.Size(221, 26);
            this.colorDetectionToolStripMenuItem.Text = "Color Detection";
            this.colorDetectionToolStripMenuItem.Click += new System.EventHandler(this.colorDetectionToolStripMenuItem_Click);
            // 
            // pickColorToolStripMenuItem
            // 
            this.pickColorToolStripMenuItem.Name = "pickColorToolStripMenuItem";
            this.pickColorToolStripMenuItem.Size = new System.Drawing.Size(183, 26);
            this.pickColorToolStripMenuItem.Text = "Pick Color";
            this.pickColorToolStripMenuItem.Click += new System.EventHandler(this.pickColorToolStripMenuItem_Click);
            // 
            // pressToDetectToolStripMenuItem
            // 
            this.pressToDetectToolStripMenuItem.Name = "pressToDetectToolStripMenuItem";
            this.pressToDetectToolStripMenuItem.Size = new System.Drawing.Size(183, 26);
            this.pressToDetectToolStripMenuItem.Text = "Press to Detect";
            this.pressToDetectToolStripMenuItem.Click += new System.EventHandler(this.pressToDetectToolStripMenuItem_Click);
            // 
            // writeTextInImageToolStripMenuItem
            // 
            this.writeTextInImageToolStripMenuItem.Name = "writeTextInImageToolStripMenuItem";
            this.writeTextInImageToolStripMenuItem.Size = new System.Drawing.Size(221, 26);
            this.writeTextInImageToolStripMenuItem.Text = "Write Text In Image";
            this.writeTextInImageToolStripMenuItem.Click += new System.EventHandler(this.writeTextInImageToolStripMenuItem_Click);
            // 
            // invertColorToolStripMenuItem
            // 
            this.invertColorToolStripMenuItem.Name = "invertColorToolStripMenuItem";
            this.invertColorToolStripMenuItem.Size = new System.Drawing.Size(221, 26);
            this.invertColorToolStripMenuItem.Text = "Invert Color";
            this.invertColorToolStripMenuItem.Click += new System.EventHandler(this.invertColorToolStripMenuItem_Click);
            // 
            // gammaToolStripMenuItem
            // 
            this.gammaToolStripMenuItem.Name = "gammaToolStripMenuItem";
            this.gammaToolStripMenuItem.Size = new System.Drawing.Size(221, 26);
            this.gammaToolStripMenuItem.Text = "Gamma";
            this.gammaToolStripMenuItem.Click += new System.EventHandler(this.gammaToolStripMenuItem_Click);
            // 
            // viewImageDetailsToolStripMenuItem
            // 
            this.viewImageDetailsToolStripMenuItem.Name = "viewImageDetailsToolStripMenuItem";
            this.viewImageDetailsToolStripMenuItem.Size = new System.Drawing.Size(221, 26);
            this.viewImageDetailsToolStripMenuItem.Text = "View Image Details";
            this.viewImageDetailsToolStripMenuItem.Click += new System.EventHandler(this.viewImageDetailsToolStripMenuItem_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(383, 65);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(619, 428);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // SelectedColorName_label
            // 
            this.SelectedColorName_label.AutoSize = true;
            this.SelectedColorName_label.Location = new System.Drawing.Point(485, 711);
            this.SelectedColorName_label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.SelectedColorName_label.Name = "SelectedColorName_label";
            this.SelectedColorName_label.Size = new System.Drawing.Size(116, 17);
            this.SelectedColorName_label.TabIndex = 3;
            this.SelectedColorName_label.Text = "Cor Selectionada";
            this.SelectedColorName_label.Click += new System.EventHandler(this.label2_Click);
            // 
            // panelSelectedColor
            // 
            this.panelSelectedColor.Location = new System.Drawing.Point(0, 33);
            this.panelSelectedColor.Margin = new System.Windows.Forms.Padding(4);
            this.panelSelectedColor.Name = "panelSelectedColor";
            this.panelSelectedColor.Size = new System.Drawing.Size(267, 123);
            this.panelSelectedColor.TabIndex = 4;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(611, 711);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(132, 22);
            this.textBox1.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(752, 715);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(249, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Number Of Pixels of the selected color";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(36, 683);
            this.textBox2.Margin = new System.Windows.Forms.Padding(4);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(132, 22);
            this.textBox2.TabIndex = 7;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 663);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(157, 17);
            this.label2.TabIndex = 8;
            this.label2.Text = "Gamma Value For Input";
            this.label2.Click += new System.EventHandler(this.label2_Click_1);
            // 
            // Lbl_Brightness
            // 
            this.Lbl_Brightness.AutoSize = true;
            this.Lbl_Brightness.Location = new System.Drawing.Point(380, 519);
            this.Lbl_Brightness.Name = "Lbl_Brightness";
            this.Lbl_Brightness.Size = new System.Drawing.Size(75, 17);
            this.Lbl_Brightness.TabIndex = 9;
            this.Lbl_Brightness.Text = "Brightness";
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(461, 500);
            this.trackBar1.Maximum = 255;
            this.trackBar1.Minimum = -255;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(488, 56);
            this.trackBar1.TabIndex = 10;
            this.trackBar1.TickFrequency = 20;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // Lbl_BrightnessValue
            // 
            this.Lbl_BrightnessValue.AutoSize = true;
            this.Lbl_BrightnessValue.Location = new System.Drawing.Point(955, 519);
            this.Lbl_BrightnessValue.Name = "Lbl_BrightnessValue";
            this.Lbl_BrightnessValue.Size = new System.Drawing.Size(16, 17);
            this.Lbl_BrightnessValue.TabIndex = 11;
            this.Lbl_BrightnessValue.Text = "0";
            // 
            // Lbl_Contrast
            // 
            this.Lbl_Contrast.AutoSize = true;
            this.Lbl_Contrast.Location = new System.Drawing.Point(380, 579);
            this.Lbl_Contrast.Name = "Lbl_Contrast";
            this.Lbl_Contrast.Size = new System.Drawing.Size(61, 17);
            this.Lbl_Contrast.TabIndex = 12;
            this.Lbl_Contrast.Text = "Contrast";
            // 
            // trackBar2
            // 
            this.trackBar2.Location = new System.Drawing.Point(461, 562);
            this.trackBar2.Maximum = 100;
            this.trackBar2.Name = "trackBar2";
            this.trackBar2.Size = new System.Drawing.Size(488, 56);
            this.trackBar2.TabIndex = 13;
            this.trackBar2.TickFrequency = 10;
            this.trackBar2.Value = 25;
            this.trackBar2.Scroll += new System.EventHandler(this.trackBar2_Scroll);
            // 
            // Lbl_ContrastValue
            // 
            this.Lbl_ContrastValue.AutoSize = true;
            this.Lbl_ContrastValue.Location = new System.Drawing.Point(956, 579);
            this.Lbl_ContrastValue.Name = "Lbl_ContrastValue";
            this.Lbl_ContrastValue.Size = new System.Drawing.Size(24, 17);
            this.Lbl_ContrastValue.TabIndex = 14;
            this.Lbl_ContrastValue.Text = "25";
            // 
            // FinalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 864);
            this.Controls.Add(this.Lbl_ContrastValue);
            this.Controls.Add(this.trackBar2);
            this.Controls.Add(this.Lbl_Contrast);
            this.Controls.Add(this.Lbl_BrightnessValue);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.Lbl_Brightness);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.panelSelectedColor);
            this.Controls.Add(this.SelectedColorName_label);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FinalForm";
            this.Text = "FinalForm";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zoomToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem colorFilterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redFilterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem blueFilterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem greenFilterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chromakeyRemoverToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem degreesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem degreesToolStripMenuItem1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem colorDetectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pickColorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pressToDetectToolStripMenuItem;
        private System.Windows.Forms.Label SelectedColorName_label;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Panel panelSelectedColor;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ToolStripMenuItem writeTextInImageToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem invertColorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gammaToolStripMenuItem;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem viewImageDetailsToolStripMenuItem;
        private System.Windows.Forms.Label Lbl_Brightness;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Label Lbl_BrightnessValue;
        private System.Windows.Forms.Label Lbl_Contrast;
        private System.Windows.Forms.TrackBar trackBar2;
        private System.Windows.Forms.Label Lbl_ContrastValue;
    }
}